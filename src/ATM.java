import java.util.concurrent.Semaphore;

public class ATM extends Thread {
    private String nombre2;
    //Variable tipo estatica, representa la cantidad de cajeros disponibles
    private static Semaphore availableATMs = new Semaphore(5); // 3 cajeros disponibles
    //constructor
    public ATM(String name) {
        this.nombre2 = nombre2;
    }

    @Override
    public void run() {
        try {
            availableATMs.acquire(); // Intenta adquirir un permiso del semáforo
            System.out.println("ATM " + nombre2 + " está en uso.");
            //imprimir un mensajde que el cajero esta en uso
            int tiempoespera = (int) (Math.random() * 8 + 3); // Tiempo aleatorio entre 3 y 10 segundos
            // se genera un numero aleatorio
            Thread.sleep(tiempoespera * 1000); //hace que el hilo duerma durante el numero de milisegundos
            System.out.println("ATM " + nombre2 + " ha sido liberado.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            availableATMs.release(); // Libera el semáforo
        }
    }
}
