// Definición de la clase Supermercado
public class Supermercado {
    // Método principal
    public static void main(String[] args) {
        // Creación de cinco instancias de la clase Caja enumeradas como 1, 2, 3,4 y 5
        Caja caja1 = new Caja("1");
        Caja caja2 = new Caja("2");
        Caja caja3 = new Caja("3");

        // Creación de cinco hilos de la clase Hilo, cada uno asociado a una caja diferente,
        //con nombres de cliente y cantidad de productos específicos
        Hilo hiloCaja1 = new Hilo(caja1, "Cliente1", 4);
        Hilo hiloCaja2 = new Hilo(caja2, "Cliente2", 3);
        Hilo hiloCaja3 = new Hilo(caja3, "Cliente3", 1);

        // Inicio de la ejecución de los hilos
        hiloCaja1.start();
        hiloCaja2.start();
        hiloCaja3.start();
    }
}