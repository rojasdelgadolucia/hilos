public class Hilo extends Thread {
    // Atributos de la clase
    private Caja caja; // Representa la caja en la que se procesará la compra
    private String cliente; // Representa el nombre del cliente que realizará la compra
    private int productos; // Representa la cantidad de productos que comprará el cliente

    // Constructor de la clase Hilo, que recibe una caja, un cliente y la cantidad de productos
    public Hilo(Caja caja, String cliente, int productos) {
        this.caja = caja; // Asigna la caja recibida al atributo de la clase
        this.cliente = cliente; // Asigna el nombre del cliente recibido al atributo de la clase
        this.productos = productos; // Asigna la cantidad de productos recibida al atributo de la clase
    }
    // Método run(), que se ejecutará cuando el hilo comience a correr
    @Override
    public void run() {
        // Invoca el método procesarCompra() de la caja asociada, pasando el nombre del cliente y la cantidad de productos
        caja.procesarCompra(cliente, productos);
    }
}