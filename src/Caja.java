// Definición de la clase Caja
public class Caja {
    // Atributo para almacenar el nombre de la caja
    private String nombre;
    // Constructor de la clase Caja que recibe el nombre de la caja como parámetro
    public Caja(String nombre) {
        // Asigna el nombre recibido al atributo de la clase
        this.nombre = nombre;
    }
    // Método para procesar la compra de un cliente en la caja
    public void procesarCompra(String cliente, int productos) {
        // Imprime un mensaje indicando que la caja está recibiendo al cliente
        System.out.println("La caja " + nombre + " recibe al cliente " + cliente + ".");
        // Itera sobre cada uno de los productos que tiene el cliente
        for (int i = 0; i < productos; i++) {
            try {
                // Genera un número aleatorio entre 0 y 10 que representará el tiempo de procesamiento de cada producto
                int tiempoAleatorio = (int) (Math.random() * 11);
                // Hace que el hilo duerma durante el tiempo aleatorio generado (en milisegundos)
                Thread.sleep(tiempoAleatorio * 1000);
                // Imprime un mensaje indicando que se ha procesado un producto para el cliente en la caja
                System.out.println("Caja " + nombre + " cliente: " + cliente + " producto " + (i + 1));
            } catch (InterruptedException e) {
                // En caso de que se produzca una interrupción mientras el hilo está dormido, imprime la traza de la excepción
                e.printStackTrace();
            }
        }
    }
}

